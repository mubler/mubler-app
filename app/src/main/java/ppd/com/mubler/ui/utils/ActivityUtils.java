package ppd.com.mubler.ui.utils;

import android.app.Activity;
import android.view.inputmethod.InputMethodManager;

import static android.content.Context.INPUT_METHOD_SERVICE;

public class ActivityUtils {

    private ActivityUtils() {

    }

    public static void closeKeyboard(Activity activity) {
        if (activity.getCurrentFocus() != null &&
                activity.getCurrentFocus().getWindowToken() != null) {

            InputMethodManager inputMethodManager = (InputMethodManager) activity.getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(activity.getCurrentFocus().getWindowToken(), 0);
        }
    }

    public final static boolean isValidEmail(CharSequence target) {
        return !target.equals("") && android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
    }
}
